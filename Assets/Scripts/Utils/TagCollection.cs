﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum Tag {
    Ball = 0,
    Paddle = 2,
    Border = 3,
    GatePlayerOne = 4,
    GatePlayerTwo = 5
}

public class TagCollection : MonoBehaviour {
    [SerializeField]
    private List<Tag> _tags;

    public List<Tag> Tags { get => _tags; }

    public bool Contains(Tag tag) {
        return _tags.Contains(tag);
    }

    public bool Contains(List<Tag> tags) {
        return _tags.Any(t => tags.Contains(t));
    }
}
