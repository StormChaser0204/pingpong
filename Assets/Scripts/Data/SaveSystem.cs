﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public static class SaveSystem {
    public static void SavePlayer(PlayerData playerData) {
        var formatter = new BinaryFormatter();
        string pathPlayer = Application.persistentDataPath + "/player.sd";
        FileStream streamPlayer = new FileStream(pathPlayer, FileMode.Create);
        formatter.Serialize(streamPlayer, playerData);
        streamPlayer.Close();
    }

    public static PlayerData LoadPlayerData() {
        PlayerData player;
        string pathPlayer = Application.persistentDataPath + "/player.sd";
        if (File.Exists(pathPlayer)) {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(pathPlayer, FileMode.Open);

            player = formatter.Deserialize(stream) as PlayerData;
            stream.Close();
        }
        else {
            Debug.LogError("Save file not found in " + pathPlayer);
            return null;
        }
        return player;
    }
}
