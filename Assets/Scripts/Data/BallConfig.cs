﻿using UnityEngine;

[System.Serializable]
public class BallConfig {
    public Sprite BallSprite;
    public float BaseSpeed;
    public float MaxSpeed;
    public float ColliderRadius;
}
