﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputController : SceneSingleton<InputController> {

    private float _inputAxisXValue;

    public float InputAxisXValue { get => _inputAxisXValue; }

    public void ChangeInputAxis(float value) {
        _inputAxisXValue = value;
    }

    protected override void Init() {
    }
}
