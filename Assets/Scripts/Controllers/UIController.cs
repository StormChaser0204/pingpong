﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIController : SceneSingleton<UIController> {

    [SerializeField]
    private GameObject _tutorialWindow;
    [SerializeField]
    private TextMeshProUGUI _currentScore;
    [SerializeField]
    private TextMeshProUGUI _bestScore;

    protected override void Init() {
        EventManager.Instance.AddListener("Lose", Lose);
    }

    protected override void OnDestroy() {
        EventManager.Instance?.RemoveListener("Lose", Lose);
        base.OnDestroy();
    }

    public void UpdateScore(int currentScore, int bestScore) {
        _currentScore.text = string.Format("Current score: {0}", currentScore);
        _bestScore.text = string.Format("Best score: {0}", bestScore);
    }

    private void Lose() {
        _tutorialWindow.SetActive(true);
        Time.timeScale = 0;
    }

    public void NewBallColor() {
        EventManager.Instance.DispatchEvent("NewColor");
    }

    public void StartGame() {
        _tutorialWindow.SetActive(false);
        Time.timeScale = 1;
        EventManager.Instance.DispatchEvent("Start");
    }
}
