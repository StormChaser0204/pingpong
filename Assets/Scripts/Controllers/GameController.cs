﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : SceneSingleton<GameController> {
    [SerializeField]
    private Ball _ball;
    [SerializeField]
    private GameObject _paddleOne;
    [SerializeField]
    private GameObject _paddleTwo;
    [SerializeField]
    private List<BallConfig> _ballConfigs;

    private int _currentScore;
    private int _bestScore;
    private Color _currentColor;

    private PlayerData _playerData;

    protected override void Init() {
        EventManager.Instance.AddListener("Bounce", IncreaseCurrentScore);
        EventManager.Instance.AddListener("NewColor", NewColor);
        EventManager.Instance.AddListener("Start", StartGame);
        EventManager.Instance.AddListener("Lose", Lose);
        _playerData = LoadPlayerData();
        _bestScore = _playerData.BestScore;
        _currentColor = _playerData.BallColor.GetColor();
        var ballSettingfs = GetRandomBallConfig();
        _ball.SetBallSettings(ballSettingfs);
        _ball.SetColor(_currentColor);
        UIController.Instance.UpdateScore(_currentScore, _bestScore);
    }

    protected override void OnDestroy() {
        EventManager.Instance?.RemoveListener("Bounce", IncreaseCurrentScore);
        EventManager.Instance?.RemoveListener("NewColor", NewColor);
        EventManager.Instance?.RemoveListener("Start", StartGame);
        EventManager.Instance?.RemoveListener("Lose", Lose);
        base.OnDestroy();
    }

    private void OnApplicationQuit() {
        SaveSystem.SavePlayer(_playerData);
    }

    public void SavePlayerData() {
        _playerData.BestScore = _bestScore;
        _playerData.BallColor = new ColorData(_currentColor);
        SaveSystem.SavePlayer(_playerData);
    }

    private PlayerData LoadPlayerData() {
        var playerData = SaveSystem.LoadPlayerData();
        if (playerData != null) return playerData;
        playerData = new PlayerData(0, ColorData.baseColor);
        SaveSystem.SavePlayer(playerData);
        return playerData;
    }

    private void IncreaseCurrentScore() {
        _currentScore++;
        UIController.Instance.UpdateScore(_currentScore, _bestScore);
    }

    private void NewColor() {
        var newColor = UnityEngine.Random.ColorHSV();
        _currentColor = newColor;
        _ball.SetColor(_currentColor);
        SavePlayerData();
    }

    private void StartGame() {
        _ball.StartGame();
    }

    private void Lose() {
        if (_currentScore > _bestScore) _bestScore = _currentScore;
        _currentScore = 0;
        UIController.Instance.UpdateScore(_currentScore, _bestScore);
        SavePlayerData();
        ReturnObjectToBasePosition();
        var ballSettingfs = GetRandomBallConfig();
        _ball.SetBallSettings(ballSettingfs);
    }

    private void ReturnObjectToBasePosition() {
        _ball.transform.position = new Vector3(0, 0, 0);
        _ball.DisableMoving();
        _paddleOne.transform.position = new Vector3(0, -5, 0);
        _paddleTwo.transform.position = new Vector3(0, 5, 0);
    }

    private BallConfig GetRandomBallConfig() {
        return _ballConfigs[UnityEngine.Random.Range(0, _ballConfigs.Count - 1)];
    }
}
