﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData {
    public int BestScore;
    public ColorData BallColor;


    public void SetColor(float r, float g, float b, float a) {
        BallColor.R = r;
        BallColor.G = g;
        BallColor.B = b;
        BallColor.A = a;
    }

    public PlayerData(int bestScore, ColorData ballColor) {
        BestScore = bestScore;
        BallColor = ballColor;
    }
}

[System.Serializable]
public struct ColorData {
    public float R;
    public float G;
    public float B;
    public float A;

    public static ColorData baseColor { get { return new ColorData(1, 1, 1, 1); } }

    //public ColorData() {
    //    R = 1;
    //    G = 1;
    //    B = 1;
    //    A = 1;
    //}

    public Color GetColor() {
        return new Color(R, G, B, A);
    }

    public ColorData(float r, float g, float b, float a) {
        R = r;
        G = g;
        B = b;
        A = a;
    }
    public ColorData(Color color) {
        R = color.r;
        G = color.g;
        B = color.b;
        A = color.a;
    }
}
