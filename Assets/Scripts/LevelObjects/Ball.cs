﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    [SerializeField]
    private float _baseSpeed;
    [SerializeField]
    private float _increaseSpeedValue;
    [SerializeField]
    private float _maxSpeed;
    [SerializeField]
    private List<Tag> _bounceTags;
    [SerializeField]
    private SpriteRenderer _spriteRenderer;
    [SerializeField]
    private CircleCollider2D _circleCollider;

    private float _currentSpeed;
    private bool _canMove;
    private Rigidbody2D _rigidbody;
    private Vector2 _direction;

    public void StartGame() {
        var randomRotation = Random.Range(30, 150); //45, 135
        transform.rotation = Quaternion.Euler(0, 0, randomRotation);
        _direction = transform.right;
        _currentSpeed = _baseSpeed;
        _rigidbody = GetComponent<Rigidbody2D>();
        _canMove = true;
    }

    public void SetBallSettings(BallConfig config) {
        _spriteRenderer.sprite = config.BallSprite;
        _baseSpeed = config.BaseSpeed;
        _maxSpeed = config.MaxSpeed;
        _circleCollider.radius = config.ColliderRadius;
    }

    public void OnCollisionEnter2D(Collision2D col) {
        var tagCollection = col.gameObject.GetComponent<TagCollection>();
        foreach (var tag in tagCollection.Tags) {
            switch (tag) {
                case Tag.Paddle:
                    EventManager.Instance.DispatchEvent("Bounce");
                    ChangeDirection(col);
                    break;
                case Tag.Border:
                    ChangeDirection(col);
                    break;
                default:
                    break;
            }
        }
    }

    private void ChangeDirection(Collision2D col) {
        Vector2 normal = col.contacts[0].normal;
        _direction = Vector2.Reflect(_rigidbody.velocity, normal).normalized;
        _rigidbody.velocity = _direction * _currentSpeed;
        IncreaseSpeed();
    }

    public void DisableMoving() {
        _canMove = false;
    }

    public void SetColor(Color color) {
        _spriteRenderer.color = color;
    }

    private void IncreaseSpeed() {
        _currentSpeed += _increaseSpeedValue;
        if (_currentSpeed > _maxSpeed)
            _currentSpeed = _maxSpeed;
    }

    private void Update() {
        if (!_canMove) return;
        _rigidbody.velocity = _direction * _currentSpeed;
    }
}
