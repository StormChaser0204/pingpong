﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _maxDistance;

    public void Update() {

#if UNITY_EDITOR
        transform.position += new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * _speed, 0, 0);
#else
        transform.position += new Vector3(InputController.Instance.InputAxisXValue * Time.deltaTime * _speed, 0, 0);
#endif
        if (transform.position.x > 7.5f) {
            transform.position = new Vector3(7.5f, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -7.5f) {
            transform.position = new Vector3(-7.5f, transform.position.y, transform.position.z);
        }
    }
}
