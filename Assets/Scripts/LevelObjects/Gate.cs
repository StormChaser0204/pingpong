﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour {
    [SerializeField]
    private TagCollection _tagCollection;


    public void OnCollisionEnter2D(Collision2D collision) {
        EventManager.Instance.DispatchEvent("Lose");
    }
}
